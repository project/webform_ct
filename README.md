# Webform Conversion-Tracking

## IMPORTANT NOTES
- **This module could have potential security issues**!
  - The currently used logic to execute JavaScript on the confirmation page
  needs a security review. Please help to find the best possible implementation!
  - Visit https://www.drupal.org/project/webform_ct/issues/3248691 for more
  information.
- **This module only works with Confirmation type "Page" and "Inline"!**
  - Visit https://www.drupal.org/project/webform_ct/issues/3314878 for more
  information.

## General
Provides the ability to execute custom JavaScript on the webform confirmation
page, typically used for conversion tracking.

## Solved Problem & Use-case
In many cases, like for ads and statistics, you may need to run a conversion
tracking script for each successfully submitted form. Typically this is done by
JavaScript event snippets provided by the ad companies to include on the
confirmation page. Anyway JavaScript is being filtered out on the Webform
success message and while Webform provides the "Custom JavaScript" textarea for
the whole form and all webform pages, it doesn't provide such a functionality
just for the confirmation page / message. Also it currently misses a good way to
detect that kind of page.

### Background
Based on the closed (won't fix) discussion in
https://www.drupal.org/project/webform/issues/2985633, we created this module to
provide a simple alternative for site admins to execute a custom (e.g.
conversion tracking code) JavaScript on the form confirmation page.

## Security
Being able to set custom JavaScripts is potentially dangerous. So be careful who
you grant the permission to administer the JavaScript or change the
configuration (where the JS is stored).

## Installation
- Require the module and dependency via
  `composer require drupal/webform drupal/webform_ct`.
- Install the module using `drush en webform_ct`.

## Usage
- Go to the webform, you would like to apply the custom confirmation javascript
code to and edit the webform's settings.
(/admin/structure/webform/manage/my-webform/settings)
- Click the "Confirmation" Tab and select either the "Page" or "Inline"
confirmation type.
- Once selected a "Confimation Javascript Code" text-field will appear on the
bottom of the form.
- Type in your custom javascript code and save.
- Done! Now your custom javascript will execute on every webform submit!
