<?php

namespace Drupal\Tests\webform_ct\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Checks escaping for custom webform_ct functionality.
 *
 * @group webform_ct_tests
 */
class WebformCtFunctionalTests extends BrowserTestBase {

  /**
   * An admin user with all permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * User without 'administer_webform_confirmation_javascript' permission.
   *
   * @var \Drupal\user\Entity\User
   */

  protected $user;
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'webform',
    'webform_ct',
  ];

  /**
   * A webform object.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->createAdminRole('administrator', 'administrator');
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole('administrator');
    $this->adminUser->save();
    $this->user = $this->createUser(['access any webform configuration'], 'user', 'user');
    $this->drupalLogin($this->adminUser);

    // Initializing the webform object with the "contact" webform, because
    // creating a webform through the "WebformBrowserTestTrait->createWebform()"
    // leads to problems (Unable to display this webform. Please contact the
    // site administrator.):
    $this->webform = \Drupal::entityTypeManager()->getStorage('webform')->load('contact');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypePage() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'page');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypeUrl() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'url');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypeUrlMessage() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'url_message');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypeInline() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'inline');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypeMessage() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'message');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if user script input in the message confirmation textarea is escaped.
   */
  public function testMessageConfirmationPageEscapesScriptTypeModal() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'modal');
    $page->fillField('confirmation_message[value]', '<script> this is very specific string </script>');
    $page->pressButton('edit-submit');
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $webformId . '/confirmation');
    $session->statusCodeEquals('200');
    $session->pageTextNotContains('<script> this is very specific string </script>');
    $session->pageTextContains('this is very specific string');
  }

  /**
   * Tests if the javascript code textarea exists.
   */
  public function testConfirmationJavascriptTextareaExists() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and input javascript in confirmation message:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $page->fillField('edit-confirmation-type-page', 'page');
    $session->elementExists('css', '#edit-confirmation-custom-javascript');
  }

  /**
   * Tests if user with permission can see the code textarea.
   */
  public function testUserWithPermissionCanSeeCodeTextarea() {
    $session = $this->assertSession();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and see if the javascript textarea is there:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $session->elementExists('css', 'textarea#edit-confirmation-custom-javascript--2');
  }

  /**
   * Tests if user without permission can not see the code textarea.
   */
  public function testUserWithoutPermissionCanNotSeeCodeTextarea() {
    $this->drupalLogout();
    $this->drupalLogin($this->user);
    $session = $this->assertSession();
    $webformId = $this->webform->getOriginalId();
    // Go to settings page and see if the javascript textarea is not there:
    $this->drupalGet('admin/structure/webform/manage/' . $webformId . '/settings/confirmation');
    $session->statusCodeEquals('200');
    $session->elementNotExists('css', 'textarea#edit-confirmation-custom-javascript');
  }

}
