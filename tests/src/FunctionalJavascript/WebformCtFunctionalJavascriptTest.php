<?php

namespace Drupal\Tests\webform_ct\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\webform\WebformInterface;

/**
 * Checks escaping for custom webform_ct functionality with javascript enabled.
 *
 * @group webform_ct_tests
 */
class WebformCtFunctionalJavascriptTest extends WebDriverTestBase {

  /**
   * An admin user with all permissions.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * User without 'administer_webform_confirmation_javascript' permission.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A webform object.
   *
   * @var \Drupal\webform\WebformInterface
   */
  protected $webform;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'webform',
    'webform_ct',
  ];

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->createAdminRole('administrator', 'administrator');
    $this->adminUser = $this->createUser([]);
    $this->adminUser->addRole('administrator');
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);

    // Initializing the webform object with the "contact" webform, because
    // creating a webform through the "WebformBrowserTestTrait->createWebform()"
    // leads to problems (Unable to display this webform. Please contact the
    // site administrator.):
    $this->webform = \Drupal::entityTypeManager()->getStorage('webform')->load('contact');
  }

  /**
   * Tests if code in the javascript textarea is not escaped.
   */
  public function testLocalStorageJavascriptTextareaNotEscaped() {
    /**
     * @var \Behat\Mink\Driver\Selenium2Driver $driver
     */
    $driver = $this->getSession()->getDriver();
    $page = $this->getSession()->getPage();

    // Set the webform settings with the alert:
    $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_PAGE);
    $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>localStorage.setItem('testkey', 'testname');</script>");
    $this->webform->save();
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
    $page->pressButton('edit-actions-submit');

    $lsValue = $driver->getWebDriverSession()->local_storage()->getKey('testkey');
    $this->assertEquals('testname', $lsValue);
  }

  /**
   * Tests if the javascript gets executed on page confirmation.
   */
  public function testExecuteJavascriptOnConfirmationPage() {
    /**
     * @var \Behat\Mink\Driver\Selenium2Driver $driver
     */
    $driver = $this->getSession()->getDriver();
    $page = $this->getSession()->getPage();
    // Set the webform settings with the alert:
    $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_PAGE);
    $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
    $this->webform->save();
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
    $page->pressButton('edit-actions-submit');
    $message = $driver->getWebDriverSession()->getAlert_text();
    $driver->getWebDriverSession()->accept_alert();
    $this->assertEquals('test', $message);
  }

  /**
   * Tests if the javascript gets executed on inline confirmation.
   */
  public function testExecuteJavascriptOnConfirmationInline() {
    /**
     * @var \Behat\Mink\Driver\Selenium2Driver $driver
     */
    $driver = $this->getSession()->getDriver();
    $page = $this->getSession()->getPage();
    // Set the webform settings with the alert:
    $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_INLINE);
    $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
    $this->webform->save();
    // Go to confirmation page and see if the script got escaped:
    $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
    $page->pressButton('edit-actions-submit');
    $message = $driver->getWebDriverSession()->getAlert_text();
    $driver->getWebDriverSession()->accept_alert();
    $this->assertEquals('test', $message);
  }

  // @codingStandardsIgnoreStart
  // @todo These are testing confirmation types, which are currently not
  // supported! Comment them in, once
  // https://www.drupal.org/project/webform_ct/issues/3314878 is fixed.
  // /**
  //  * Tests if the javascript gets executed on message confirmation.
  //  */
  // public function testExecuteJavascriptOnConfirmationMessage() {
  //   /**
  //    * @var \Behat\Mink\Driver\Selenium2Driver $driver
  //    */
  //   $driver = $this->getSession()->getDriver();
  //   $page = $this->getSession()->getPage();
  //   // Set the webform settings with the alert:
  //   $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_MESSAGE);
  //   $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
  //   $this->webform->save();
  //   // Go to confirmation page and see if the script got escaped:
  //   $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
  //   $page->pressButton('edit-actions-submit');
  //   $message = $driver->getWebDriverSession()->getAlert_text();
  //   $driver->getWebDriverSession()->accept_alert();
  //   $this->assertEquals('test', $message);
  // }

  // /**
  //  * Tests if the javascript gets executed on modal confirmation.
  //  */
  // public function testExecuteJavascriptOnConfirmationModal() {
  //   /**
  //    * @var \Behat\Mink\Driver\Selenium2Driver $driver
  //    */
  //   $driver = $this->getSession()->getDriver();
  //   $page = $this->getSession()->getPage();
  //   // Set the webform settings with the alert:
  //   $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_MODAL);
  //   $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
  //   $this->webform->save();
  //   // Go to confirmation page and see if the script got escaped:
  //   $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
  //   $page->pressButton('edit-actions-submit');
  //   $message = $driver->getWebDriverSession()->getAlert_text();
  //   $driver->getWebDriverSession()->accept_alert();
  //   $this->assertEquals('test', $message);
  // }

  // /**
  //  * Tests if the javascript gets executed on url confirmation.
  //  */
  // public function testExecuteJavascriptOnConfirmationUrl() {
  //   /**
  //    * @var \Behat\Mink\Driver\Selenium2Driver $driver
  //    */
  //   $driver = $this->getSession()->getDriver();
  //   $page = $this->getSession()->getPage();
  //   // Set the webform settings with the alert:
  //   $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_URL);
  //   $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
  //   $this->webform->save();
  //   // Go to confirmation page and see if the script got escaped:
  //   $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
  //   $page->pressButton('edit-actions-submit');
  //   $message = $driver->getWebDriverSession()->getAlert_text();
  //   $driver->getWebDriverSession()->accept_alert();
  //   $this->assertEquals('test', $message);
  // }

  // /**
  //  * Tests if the javascript gets executed on url message confirmation.
  //  */
  // public function testExecuteJavascriptOnConfirmationUrlMessage() {
  //   /**
  //    * @var \Behat\Mink\Driver\Selenium2Driver $driver
  //    */
  //   $driver = $this->getSession()->getDriver();
  //   $page = $this->getSession()->getPage();
  //   // Set the webform settings with the alert:
  //   $this->webform->setSetting('confirmation_type', WebformInterface::CONFIRMATION_URL_MESSAGE);
  //   $this->webform->setThirdPartySetting('webform_ct', 'confirmation_custom_javascript', "<script>alert('test');</script>");
  //   $this->webform->save();
  //   // Go to confirmation page and see if the script got escaped:
  //   $this->drupalGet('/webform/' . $this->webform->getOriginalId() . '/test');
  //   $page->pressButton('edit-actions-submit');
  //   $message = $driver->getWebDriverSession()->getAlert_text();
  //   $driver->getWebDriverSession()->accept_alert();
  //   $this->assertEquals('test', $message);
  // }
  // @codingStandardsIgnoreEnd

}
